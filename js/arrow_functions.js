const regards = function () {
  console.log("Hi!");
};
const regards2 = () => console.log("Hi2!");

const regards3 = (name) => console.log(`Hi2 ${name}!`);

const sum = function (a, b) {
  return a + b;
};
const sum2 = (a, b) => a + b;

console.log(sum(1, 2));
console.log(sum2(1, 4));

regards();
regards2();
regards3("Michael");

const large = () => {
  console.log(1);
  console.log(2);
  console.log(3);
};
large();

const numbers = [100, 200, 300, 400, 500];
numbers.forEach(function (n, index) {
  console.log(`Elemento ${n}, ubicado en la posición ${index}.`);
});
console.log("*************************************+");
numbers.forEach((n, index) =>
  console.log(`Elemento ${n}, ubicado en la posición ${index}.`)
);

function dog() {
  console.log(this);
}
//dog();

const cat2 = {
  name: "zumba",
  age: 3,
  miaugar: function () {
    console.log(this);
  },
  miaugar1: () => console.log(this),
};

const cat = {
  name: "zumba",
  age: 3,
  miaugar () {
    console.log(this);
  },
  miaugar1: () => console.log(this),
  cat2,
};
/*cat.miaugar();
cat.miaugar1();*/
cat.miaugar();
