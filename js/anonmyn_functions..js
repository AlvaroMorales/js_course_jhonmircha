//Función anónima autoejecutable.
//alert("Hey");
(function () {
  console.log("Mi pan IIFE Inmediatly Involved function exutable");
})();
(function (d, w, c) {
  console.log("Mi pan IIFE Inmediatly Involved function exutable 2");
  console.log(d);
  console.log(w);
  c.log("My console.log.");
})(document, window, console);

//Clásica.
(function () {
  console.log("Versión Classic");
})();
//La Crockford (Douglas Crockford, creador del format Json) 
// (JS the good parts).
((function () {
  console.log("Versión Crockford");
})());
//Unaria
+function(){
    console.log("Versión unaria");
}();
//Facebook
!function(){
    console.log("Versión Facebook");
}();

