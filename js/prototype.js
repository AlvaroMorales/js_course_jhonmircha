/*
Clases- Modelo a seguir.
Objetos- Instancia de una clase.
    Atributos
    Métodos
*/
const myObject1 = {
  name: "MyName",
  make() {
    console.log();
  },
};

const myObject2 = {
  name: "MyName2",
  make() {
    console.log();
  },
};
/*
console.log(myObject1);
console.log(myObject2);

//Función constructora.
function Animal(name, gender) {
  this.name = name;
  this.gender = gender;

  this.make = function () {
    console.log("Jao");
  };
  this.sayingHello = function () {
    console.log(`My name's ${name}`);
  };
}
const myObjectTest = new Animal("Cat", "0");

console.log(myObjectTest);

const myObjectTest2 = new Animal("Dog", 1);

console.log(myObjectTest2);
myObjectTest.sayingHello();
*/

function Animal(name, gender) {
  this.name = name;
  this.gender = gender;

  this.make = function () {
    console.log("Jao");
  };
  this.sayingHello = function () {
    console.log(`My name's ${name}`);
  };
}
const myAnminal1= new Animal('Diego',0);
const myAnminal2= new Animal('Diego 2',0);
const myAnminal3= new Animal('Diego 3',0);
console.log(myAnminal1);
console.log(myAnminal2);
console.log(myAnminal3);

/*Función constructora donde asignamos métodos al prototipo, 
no a la función como tal.*/
function AnimalWithoutFunctions(name, gender) {
  this.name = name;
  this.gender = gender;
}
//Métodos agregados al prototipo de la función constructura.
AnimalWithoutFunctions.prototype.make = function () {
  console.log("Jao");
};
AnimalWithoutFunctions.prototype.sayingHello = function () {
  console.log(`My name's ${this.name}`);
};
const myObjectTest = new AnimalWithoutFunctions("Cat", "0");

console.log(myObjectTest);

const myObjectTest2 = new AnimalWithoutFunctions("Dog", 1);

console.log(myObjectTest2);
myObjectTest.make();
myObjectTest.sayingHello();
myObjectTest2.sayingHello();