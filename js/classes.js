class Animal {
  constructor(name, gender) {
    this.name = name;
    this.gender = gender;
  }
  myGender = () => console.log(`My gender is ${this.gender}`);
}
const mimi = new Animal("Mimi", 0),
  stuart = new Animal("Stuart", 0);

console.log(mimi);
console.log(stuart);

class Dog extends Animal {
  constructor(name, gender, size) {
    super(name, gender);
    this.size = size;
  }
  bark = () => console.log("I am barking!");
  sayingHi = () => console.log(`My name's ${this.name}`);
}
const scooby = new Dog("Scooby", 0, 100);
const scooby2 = new Dog("Scooby2", 0, 100);

console.log(scooby);
console.log(scooby2);

scooby.sayingHi();
