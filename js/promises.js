function squiredPromise(value) {
  if (typeof value != "number") 
  return Promise.reject("Value is nan.");

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        value,
        result: value * value,
      });
    }, Math.random() * 2000);
  });
}

squiredPromise(0)
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
  })
  .catch();
squiredPromise(1)
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
    return squiredPromise(1);
  })
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
    return squiredPromise(2);
  })
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
    return squiredPromise('sdf');
  })
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
    return squiredPromise(4);
  })
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
    return squiredPromise(5);
  })
  .then((obj) => {
    console.log(`Promise ${obj.value}`);
    console.log(obj);
  })
  .catch((err) => console.error(err));
