const numbers = [10, 20, 30, 40, 50, 60, 70];

for (let i = 0; i < numbers.length; i++) {
  if (i === 5) {
    break;
  }
  console.log(`${numbers[i]}, ${i}`);
}
//Continueeeee
for (let i = 0; i < numbers.length; i++) {
  if (i === 5) {
    continue;
  }
  console.log(`${numbers[i]}, ${i}`);
}
