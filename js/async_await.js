function squiredPromise(value) {
  if (typeof value !== "number") return new Promise.reject(console.log("Nan"));
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        value,
        result: value * value,
      });
    }, Math.random() * 1000);
  });
}
async function asyncDeclaredFunction() {
  try {
    console.log("Starting async function...");
    let obj = await squiredPromise(0);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(1);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(2);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(3);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise("234e");
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(5);
    console.log(`Async function ${obj.value},${obj.result}`);
  } catch (error) {
    console.error(error);
  }
}
//Función asíncrona expresada.
const asyncDeclaredFunction2 = async () => {
  try {
    console.log("Starting async function...");
    let obj = await squiredPromise(6);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(7);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(8);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(9);
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise("234e");
    console.log(`Async function ${obj.value},${obj.result}`);
    obj = await squiredPromise(11);
    console.log(`Async function ${obj.value},${obj.result}`);
  } catch (error) {
    console.error(error);
  }
};
asyncDeclaredFunction();
asyncDeclaredFunction2();
