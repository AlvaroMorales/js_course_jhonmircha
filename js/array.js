const a=[];

const b= [1,true,"Hi",["Hi1","Hi2",["Hi11","Hi22"]]];

console.log(a);
console.log(b);
console.log(b[3][2][1]);

const c = Array.of("1",2,true);
console.log(c);

const d= Array(100).fill(3);

console.log(d[2]);
const colors=["Red","Green","Blue"];
colors.push("Black");
console.log(colors);
colors.pop();
console.log(colors);

colors.forEach(function (e,index) {
  console.log(`<li id="${index}">${e}</li>`);  
});



