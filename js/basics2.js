// ||= Si el valor de la izquierda valida a true, se toma ese valor.
// &&= Si el valor de la izquierda valida a false, se toma ese valor.


function hi(name) {
  name = name || "Unknownnn";
  console.log(`Hi ${name}`);
}
hi();
hi("Me");

console.log("Hi2" || "Hi1");
console.log([] || "Hi1");
console.log({} || "Hi1");
console.log(false || "Hi1");
console.log(null || "Hi1");
console.log(undefined || "Hi1");
console.log(true || "Hi1");
console.log(0 || "Hi1");

console.log("***********************");
console.log(undefined && "Hi1");
console.log(true && "Hi1");
console.log(0 && "Hi1");
