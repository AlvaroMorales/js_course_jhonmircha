//* transforma la función en un generador.
function* iterable() {
  yield "Hi";
  console.log("Hi console");
  yield "h2";
  console.log("We continue");
  yield "h3";
  yield "h4";
}

let iterableVar = iterable();

for (let y of iterableVar) {
  console.log(y);
}

const arr = [...iterable()];

console.log(arr);

function squired(value) {
  setTimeout(() => {
    return console.log({ value, result: value * value });
  }, Math.random() * 1000);
}
function* generator() {
  console.log("Starting generator...");
  yield squired(0);
  yield squired(1);
  yield squired(2);
  yield squired(3);
  yield squired(4);
  yield squired(5);
  console.log("Ending generator.");
}
let gen = generator();
for (const y of gen) {
  console.log(y);
}
