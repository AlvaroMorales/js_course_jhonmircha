const person = {
  name: "",
  lastName: "",
  nickName: "",
};

const handlerr = {
  set(obj, prop, value) {
    if (Object.keys(obj).indexOf(prop) === -1) {
      console.log(`${prop} no existe.`);
      return false;
    }
    if (
      (prop === "name" || prop === "lastName") &&
      !/^[A-Za-zÑñ\s]+$/g.test(value)
    ) {
      console.log("Testing regular expression.");
    }
    obj[prop] = value;
    return true;
  },
};
const marcoss = new Proxy(person, handlerr);
marcoss.name = "Marco";
marcoss.lastName = "3";
marcoss.nickName = "MariumnK";
marcoss.twitter = "ajfji@afosff.com";

console.log(marcoss);
console.log(person);
