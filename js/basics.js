const numbers = [1,2,3];

let one = numbers[0];
let lastOne = numbers[numbers.length-1];
console.log(one);
console.log(lastOne);

//Destructuración.
console.log("Destructuración");
const[uno,two,three] = numbers;
console.log(uno);
console.log(two);
console.log(three);

const person= {name: "namess",
lastName: "LastNamess",
age:13};
let {name,age,lastName}=person;
console.log(name);
console.log(lastName);
console.log(age);

