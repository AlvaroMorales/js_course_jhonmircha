console.log(console);
console.error("OMG!");
console.warn("I'm telling you");
console.info("Testing info");
console.log("Testing info");
let name = "MyName",
  lastName = "MyLastName";

console.log(name, lastName);

console.log(`Hi my name is ${name}`);

console.log("Hi my name is %% and my lastname is %%");

console.log("Hi my name is %s and my lastname is %s");

console.log("Hi my name is %s and my lastname is %s", name, lastName);

console.clear();

console.log(document);

console.log(window);
console.dir(window);
console.dir(document);

console.groupCollapsed("Cursos chingones");
console.log("Test1");
console.log("Test2");
console.log("Test3");
console.log("Test4");
console.log("Test5");
console.groupEnd();

console.groupCollapsed("Cursos chingones");
console.log("Test1");
console.log("Test2");
console.log("Test3");
console.log("Test4");
console.log("Test5");
console.groupEnd();
console.table(Object.entries(console).sort());

console.clear();
const numbers = [1, 2, 3, 4, 5],
  vocales = ["a", "e", "i", "o", "u"];

console.table(numbers);

console.table(vocales);

const dog = {
  name: "meh",
  lastName: "mejhl",
};
console.table(dog);

console.time("Test Time");
const myArray = Array(100);

for (let i = 0; i < myArray.length; i++) {
  myArray[i] = i;
}

for (let i = 0; i < myArray.length; i++) {
  //console.count("Code for");
  //console.log(i);
}
console.timeEnd("Test Time");

let x = 1,
  y = 2,
  xy = "E x<y";
  x=3;
  console.assert(x<y,[x,y,xy]);
