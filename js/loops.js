let i = 0;
while (i < 10) {
  console.log(i);
  i++;
}
do {
  console.log(i);
  i--;
} while (i > 0);
for(i; i<10; i++){
console.log(i);
}
let numbers= [10,20,30,40,50,60,70]

const myObjects = {
    name: "name1",
    lastName:"lastName1",
    birthdate:"ni idea"
}
//For in es más para objetos.
for(const item in myObjects){
console.log(`Key: ${item}, Value: ${myObjects[item]}`);
}
//For of es más para arrays
for(const item of numbers){
console.log(item);
}
let myString="Hi world"
for(const item of myString){
    console.log(item);
}
