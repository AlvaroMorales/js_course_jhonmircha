//Sets es un arreglo de JS que solo acepta valores únicos.
//Foreach es para arreglos y sets.
//Sets no son arreglos.
const person = {
  name: "sdf",
};

const set = new Set([1, 2, 2, 3, 4, 4, 5, person, person, {}, {}]);

const set2 = new Set();
set2.add(1);
set2.add(2);
set2.add(2);
set2.add(3);
set2.add(4);
set2.add(4);
set2.add(5);
set2.add({});
set2.add(person);
set2.add(person);

console.log(set);
console.log(set2);

set.forEach((item) => console.log(item));
console.log(set[0]);

let myNewSet= Array.from(set);
console.log(myNewSet);
console.log(myNewSet[0]);
console.log(set.has(1));
set.clear();
console.log(set);
