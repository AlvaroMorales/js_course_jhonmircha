console.log(document.documentElement.lang);

console.log(document.documentElement.getAttribute("lang"));

console.log(document.querySelector(".link-dom").href);
console.log(document.querySelector(".link-dom").getAttribute("href"));

//document.documentElement.lang="es";

document.documentElement.setAttribute("lang","es");

const $linkDom=document.querySelector(".link-dom");

$linkDom.setAttribute("target","blank");
$linkDom.setAttribute("rel","noopener");
$linkDom.setAttribute("href","https://www.youtube.com/watch?v=l6npGZa_vgc&list=PLvq-jIkSeTUZ6QgYYO3MwG9EMqC-KoLXA&index=64");
console.log($linkDom.hasAttribute("rel"));
console.log($linkDom.removeAttribute("rel"));
console.log($linkDom.hasAttribute("rel"));

//Data Attributes

console.log($linkDom.getAttribute("data-description"));
console.log($linkDom.dataset);

console.log($linkDom.dataset.description);
console.log($linkDom.dataset.id);
$linkDom.setAttribute("data-description","ñoño");

$linkDom.dataset.description="ñoñoño";
console.log($linkDom.dataset.description);

console.log($linkDom.hasAttribute("data-id"));
console.log($linkDom.removeAttribute("data-id"));
console.log($linkDom.hasAttribute("data-id"));
