document.write("<h2>This is meee.</h2>");

console.log(document.getElementsByTagName("p"));
console.log(document.getElementsByTagName("li"));

console.log(document.getElementsByClassName("card"));
console.log(document.getElementsByName("name"));

console.log(document.getElementById("menuId"));

//QuerySelector #id... solo trae el primer elemento encontrado.

console.log(document.querySelector("#menuId"));

console.log(document.querySelector("a"));
console.log(document.querySelectorAll("a"));
document.querySelectorAll("a").forEach(e=>console.log(e));
document.querySelectorAll("card").forEach(e=>console.log(e));
document.querySelectorAll(".card").forEach(e=>console.log(e));
document.querySelectorAll("#menuId li").forEach(e=>console.log(e));
