const $whatIsDom = document.getElementById("que-es");

let text = `
<p>
  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
  Vitae recusandae quos facilis enim perspiciatis? Temporibus 
  itaque nihil officiis reiciendis iure. Optio similique alias,
  quae molestias voluptatem natus maxime cum dolorum!
  (<b><i>DOM</i></b>)
</p>
<p>
  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
  Vitae recusandae quos facilis enim perspiciatis? Temporibus 
  itaque nihil officiis reiciendis iure. Optio similique alias,
  quae molestias voluptatem natus maxime cum dolorum!
</p>
<p>
  <mark>Esto va dentor del mark.</mark>
</p>
`;
/* Inner text para explorer, Text content es el estándar. */

/* $whatIsDom.innerText=text;
 */
/* $whatIsDom.textContent=text;
 */
/* $whatIsDom.outerHTML=text;
 */
$whatIsDom.innerHTML = text;
