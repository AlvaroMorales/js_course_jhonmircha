const $btnTheme = document.querySelector("#btn-theme"),
  $selectors = document.querySelectorAll("[data-dark]");

let moon = "🌙",
  sun = "☀️";

document.addEventListener("click", (e) => {
  console.log($btnTheme.textContent);
  if (e.target.matches("#btn-theme")) {
    if ($btnTheme.textContent == moon) {
      $selectors.forEach((el) => el.classList.add("dark-mode"));
      $btnTheme.textContent = sun;
      console.log("ye");
    } else {
      $selectors.forEach((el) => el.classList.remove("dark-mode"));
      $btnTheme.textContent = moon;
      console.log("ne");
    }
  }
});
