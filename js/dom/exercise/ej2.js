const $srWatch = document.getElementById("btnStartWatch"),
  $stWatch = document.getElementById("btnStopWatch"),
  $srAlarm = document.getElementById("btnStartAlarm"),
  $stAlarm = document.getElementById("btnStopAlarm"),
  $lblHour = document.getElementById("lblHour"),
  $audio = document.getElementById("testAudio");
let SEE_WATCH = false,
  clockInterval,
  clockInterval2;

$lblHour.innerText = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
$srWatch.addEventListener("click", function () {
  console.log("click");
  getHour();
});
$stWatch.addEventListener("click", () => {
  clearInterval(clockInterval);
});

$srAlarm.addEventListener("click", () => {
  $audio.play();
  clockInterval2= setInterval(() => {
    $audio.play();
  }, 100);
});
$stAlarm.addEventListener("click", () => {
  clearInterval(clockInterval2);
  $audio.pause();
});
function getHour() {
  clockInterval = setInterval(() => {
    $lblHour.innerText = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
  }, 1000);
}
