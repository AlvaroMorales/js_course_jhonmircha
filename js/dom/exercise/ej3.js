const $myBall = document.querySelector(".circle");
let x = $myBall.getBoundingClientRect().width;
let y = $myBall.getBoundingClientRect().height;
console.log($myBall.getBoundingClientRect());
var denotationX = 1,
  denotationY = 1;

setInterval(() => {
  if ((x) >= $myBall.parentElement.getBoundingClientRect().width - $myBall.clientWidth) {
    denotationX = denotationX * -1;
    console.log("1");
  }
  if (x <= 0) {
    denotationX = denotationX * -1;
  }
  if (y >= $myBall.parentElement.getBoundingClientRect().height - $myBall.getBoundingClientRect().height) {
    denotationY = denotationY * -1;
  }
  if ((y) <= 0) {
    denotationY = denotationY * -1;
  }
  x = x + denotationX;
  y = y + denotationY;

  $myBall.style.transform = `translate(${x}px,${y}px)`;

}, 1);

document.addEventListener("keydown", (e) => {
  console.log(x, y, denotationX, denotationY);
  switch (e.keyCode) {
    case 37:
      denotationX = -1;
      break;
    case 38:
      denotationY = -1;
      break;
    case 39:
      denotationX = 1;
      break;
    case 40:
      denotationY = 1;
      break;
  }
  console.log(e.keyCode);
  e.preventDefault();
});
