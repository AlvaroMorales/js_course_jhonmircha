const $myButton = document.querySelector(".scroll-top-btn");

window.addEventListener("scroll", (e) => {
  let scrollTop = window.pageYOffset || document.scrollTop;
  if (scrollTop > 600) {
    $myButton.classList.remove("hidden");
  } else {
    $myButton.classList.add("hidden");
  }
});
document.addEventListener("click", (e) => {
  if (e.target.matches(".scroll-top-btn")) {
    window.scrollTo({ behavior: "smooth", top: 0 });
  }
});
