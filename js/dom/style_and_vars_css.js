const $link = document.querySelector(".link-dom");

console.log($link.style);
console.log($link.getAttribute("style"));

console.log(window.getComputedStyle($link));

console.log(getComputedStyle($link).getPropertyValue("color"));

$link.style.setProperty("text-decoration", "none");
$link.style.setProperty("display", "block");
$link.style.width = "50%";
$link.style.textAlign = "center";
$link.style.marginLeft = "auto";
$link.style.marginRight = "auto";
$link.style.padding = "0.5rem";
$link.style.borderRadius = "1rem";

console.log($link.getAttribute("style"));

//Variables css- Custom Properties CSS
const $html= document.documentElement,
$body= document.body;

let varDarkColor= getComputedStyle($html).getPropertyValue("--dark-color"),
varYellowColor= getComputedStyle($html).getPropertyValue("--yellow-color");

console.log(varDarkColor);
console.log(varYellowColor);

$body.style.background=varDarkColor;
$body.style.color=varYellowColor;


$html.style.setProperty("--dark-color","#222333");
varDarkColor=getComputedStyle($html).getPropertyValue("--dark-color");
console.log(varDarkColor);
$body.style.setProperty("background-color",varDarkColor);

let af=[];
const ae=[];

af=["1","2"];

ae.push(["1","2"]);

console.log(af,ae);

