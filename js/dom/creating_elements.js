const $figure = document.createElement("figure"),
  $image = document.createElement("img"),
  $figCaption = document.createElement("figcaption"),
  $figCaptionText = document.createTextNode("Animals"),
  $cards = document.querySelector(".cards"),
  $figure2 = document.createElement("figure");

$image.setAttribute(
  "src",
  "https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg"
);
$image.setAttribute("alt", "Esto es una imagen.");

$figure.classList.add("card");
$figCaption.appendChild($figCaptionText);

$figure.appendChild($image);
$figure.appendChild($figCaption);
$cards.appendChild($figure);

$figure2.innerHTML = `
<img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" alt="Esto es una imagen.">
 <figcaption>people</figcaption>
 `;
$figure2.classList.add("card");

const stations = ["Primavera", "Verano", "Invierno", "Otoño"],
  $ul = document.createElement("ul");

document.write("<h3>Estaciones del año</h3>");
document.body.appendChild($ul);
stations.forEach((element) => {
  const $li = document.createElement("li");
  $li.textContent = element;
  $ul.appendChild($li);
});

const continents = ["América", "Asia", "África", "Oceanía", "Europa"],
  $ul2 = document.createElement("ul");

document.write("<h3>Continentes del Mundo</h3>");
document.body.appendChild($ul2);
continents.forEach((c) => {
  $ul2.innerHTML += `<li>${c}</li>`;
});

const crazyContinents = [
    "América",
    "Alibaba",
    "Namekusei",
    "Asia",
    "África",
    "Oceanía",
    "Europa",
  ],
  $ul3 = document.createElement("ul"),
  $fragment = document.createDocumentFragment();

crazyContinents.forEach((c) => {
  const $li = document.createElement("li");
  $li.textContent = c;
  $fragment.appendChild($li);
});

document.write("<h3>Continentes locos del Mundo</h3>");
$ul3.appendChild($fragment);
document.body.appendChild($ul3);