const $divEventFlows = document.querySelectorAll(".events-flow div"),
  $aEventsFlow = document.querySelector(".events-flow a");

console.log($divEventFlows);
console.log($aEventsFlow);

function eventFlow(e) {
  console.log(`Hi, I'm ${this.className}, click made by ${e.target.className}`);
  e.stopPropagation();
}

$divEventFlows.forEach((div) => {
  //Bubble phase
  div.addEventListener("click", eventFlow);
  // div.addEventListener("click",eventFlow,false);
  //Fin Bubble phase
  //Capture phase
  //div.addEventListener("click",eventFlow,true);
  /*   div.addEventListener("click",eventFlow,{
       capture:false,
       once:true,
   }); */
});

$aEventsFlow.addEventListener("click", (e) => {
  alert("I'm batman!");
  e.preventDefault();
  e.stopPropagation();

});
