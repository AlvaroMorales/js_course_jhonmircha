window.addEventListener("resize", (e) => {
  console.clear();
  console.log(window.innerWidth);
  console.log(window.innerHeight);
  console.log(window.outerWidth);
  console.log(window.outerHeight);
  console.log(window.scrollX);
  console.log(window.scrollY);
});

window.addEventListener("scroll",e=>{
    console.clear();
    console.log(window.scrollX);
  console.log(window.scrollY);
});
window.addEventListener("load",e=>{
    console.log("Load win");
    console.log(e);
});
//Carga de documento es más rápida que el window.
window.addEventListener("DOMContentLoaded",e=>{
    console.log("Load doc");
    console.log(e);
});
