function greet(nombre = "unknown") {
  console.log(event);
  alert(`Hi ${nombre} - ${event.target}`);
}

const $multiEvent = document.getElementById("multi-event");
const $removeEvent = document.getElementById("remove-event");

$multiEvent.addEventListener("click", () => greet("test"));

const removingDoubleClick = (e) => {
  alert(`Removing event ${e.type}`);
  console.log("");
  $removeEvent.removeEventListener("dblclick", removingDoubleClick);
  $removeEvent.disabled = true;
};

$removeEvent.addEventListener("dblclick", removingDoubleClick);
