function hiWolrd(name) {
  alert(`Hi world ${name}`);
  console.log(event);
}
const $semanticEvent = document.getElementById("semantic-event");
const $multiEvent = document.getElementById("multi-event");
$semanticEvent.onclick = hiWolrd;
$semanticEvent.onclick = function (e) {
  alert("Hi world, semantic handler!");
  console.log(e);
  console.log(event);
};
$multiEvent.addEventListener("click", hiWolrd);
$multiEvent.addEventListener("click", (e) => {
  alert("multi-event-handler");
  console.log(e.target);
  console.log(e.type);
  console.log(e);
  console.log(event);
});
