window.alert("Test");
window.confirm("Test");
window.prompt("Test");

const $btnAbrir = document.getElementById("abrir-ventana"),
  $btnCerrar = document.getElementById("cerrar-ventana"),
  $btnImprimir = document.getElementById("imprimir-ventana");

let myWindow;

$btnAbrir.addEventListener("click", (e) => {
  myWindow = window.open("https://jonmircha.com");
});
$btnCerrar.addEventListener("click", (e) => {
  myWindow.close();
});
$btnImprimir.addEventListener("click",e=>{
    window.print();
});
//Media para cargar hoja de estilo a la hora de imprimir.
