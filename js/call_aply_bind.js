console.log(this);
this.place = "Global Hi man!";
this.name="Windows";
function regret(words, who) {
  console.log(`${words} ${who} from ${this.place}`);
}
regret();
const obj = {
  place: "Object",
};

regret.call(obj, "Hi", "Nels");

regret.apply(obj, ["sdf", "erer"]);
regret.apply(null, ["sdf", "erer"]);
regret.call(null, "Hi", "Nels");

const person = {
  name: "personndfgsf",
  regret: function () {
    console.log(`Hi ${this.name}`);
  },
};
person.regret();

const otherPerson = {
  //regret: person.regret.bind(person),
  regret: person.regret.bind(this),
};
otherPerson.regret();

