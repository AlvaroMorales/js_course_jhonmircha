function squaring(value, callback) {
  setTimeout(() => {
    callback(value, value * value);
    console.log();
  }, Math.random() * 1000);
}

squaring(0, (value, result) => {
  console.log("Starting callback");
  console.log(`Callback ${value},${result}`);

  squaring(1, (value, result) => {
    console.log(`Callback ${value},${result}`);
    squaring(2, (value, result) => {
      console.log(`Callback ${value},${result}`);
      squaring(3, (value, result) => {
        console.log(`Callback ${value},${result}`);
        squaring(4, (value, result) => {
          console.log(`Callback ${value},${result}`);
          squaring(5, (value, result) => {
            console.log(`Callback ${value},${result}`);
          });
        });
      });
    });
  });
});
