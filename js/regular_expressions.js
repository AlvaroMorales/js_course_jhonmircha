let identityDocument= "O81-021097-0005G-O81";

//Banderas i:Ignora mayúsculas o minúsculas.
//Banderas g:Toma todas las coincidencias..
let myRegularExpression= new RegExp("o8","");
let myRegularExpression1= new RegExp("o8","ig");

let myRegularExpressionDiff= /o8/ig;
let myRegularExpressionDiff1= /[0-9]/ig;
let myRegularExpressionDiff2= /\d/ig;
let myRegularExpressionDiff3= /o8{1,2}/ig;

let myRegularExpressionDiff4= /o8{3,}/ig;


console.log(myRegularExpression.test(identityDocument));
console.log(myRegularExpression.exec(identityDocument));

console.log(myRegularExpression1.test(identityDocument));
console.log(myRegularExpression1.exec(identityDocument));

console.log(myRegularExpressionDiff.test(identityDocument));
console.log(myRegularExpressionDiff.exec(identityDocument));

console.log(myRegularExpressionDiff1.test(identityDocument));
console.log(myRegularExpressionDiff1.exec(identityDocument));

console.log(myRegularExpressionDiff3.test(identityDocument));
console.log(myRegularExpressionDiff3.exec(identityDocument));

console.log(myRegularExpressionDiff4.test(identityDocument));
console.log(myRegularExpressionDiff4.exec(identityDocument));

