console.log(Date());

let dateInit= new Date();
console.log(dateInit)

//Retorna el día.
console.log(dateInit.getDate());
console.log(dateInit.getDay());
console.log(dateInit.getMonth());
console.log(dateInit.getYear());
console.log(dateInit.getFullYear());
console.log(dateInit.getHours());
console.log(dateInit.getMinutes());
console.log(dateInit.getSeconds());
console.log(dateInit.getMilliseconds());
console.log(dateInit.toString());
console.log(dateInit.toDateString());
console.log(dateInit.toLocaleString());
console.log(dateInit.toLocaleDateString());
console.log(dateInit.toLocaleTimeString());
console.log(dateInit.getTimezoneOffset())
console.log(dateInit.getUTCDate())
console.log(dateInit.getUTCHours())

console.log(Date.now())

let myAge= new Date(1997,9,2);
console.log(myAge);