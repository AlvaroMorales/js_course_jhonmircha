function sum(a, b, ...c) {
  let mySum = 0;
  c.forEach(function (n) {
    mySum += n;
  });
  return mySum;
}
console.log(sum(1, 2));

const array1= [1,2,3,4,5]
const array2= [6,7,8,9,10]

console.log(array1);
console.log(array2);

const array3= [...array1,...array2]
console.log(array3);
