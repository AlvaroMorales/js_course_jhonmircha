let id = Symbol("id");
let id2 = Symbol("id");

console.log(id === id2);
console.log(id == id2);

console.log(id, id2);
console.log(typeof id, typeof id2);

const NAME = Symbol("NAME1");
const GREET = Symbol("GREET1");
//Propiedades privadas de objetos [nombreDelObjeto].
const PERSON = {
  [NAME]: "My Name pueh",
};
PERSON.NAME = "asf";
PERSON.lastName = "asf";
PERSON.NAME = "asffff";

console.log(PERSON.NAME);
console.log(PERSON[NAME]);

PERSON[GREET] = () => {
  console.log("Hi");
};
console.log(PERSON);
PERSON[GREET]();

for (const property in PERSON) {
  console.log(property);
  console.log(PERSON[property]);
}

console.log(Object.getOwnPropertySymbols(PERSON));
