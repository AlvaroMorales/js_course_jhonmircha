(() => {
  console.log("Sync code");
  console.log("Starting");
  let two = () => console.log("2");
  let one = () => {
    console.log("1");
    two();
    console.log("3");
  };
  one();
  console.log("End");
})();

(() => {
  console.log("Sync code 2");
  console.log("Starting");
  let two = () => {
    setTimeout(() => {
      console.log("2");
    }, 1000);
  };
  let one = () => {
    setTimeout(() => {
      console.log("1");
    }, 0);
    two();
    console.log("3");
  };
  one();
  console.log("End");
})();
