const myJson = {
  myString: "I am the string",
  number: 123,
  boolean: ["run", "programing", "cook"],
  myObject: {
    name: "I am the object.",
  },
};

console.log(JSON);

console.log("{}");
console.log(JSON.parse("{}"));
console.log("[1,2,3]");

console.log(JSON.parse("[1,2,3]"));
console.log(JSON.parse("false"));
console.log(JSON.parse("12"));
//console.log(JSON.parse("'Hi'"));
console.log('{"myString":"I am the string","number":123,"boolean":["run","programing","cook"],"myObject":{"name":"I am the object."}}');
console.log(JSON.parse('{"myString":"I am the string","number":123,"boolean":["run","programing","cook"],"myObject":{"name":"I am the object."}}'));
console.log(JSON.stringify(myJson));
console.log(myJson);
