console.log(this);
this.name = "Global.";

function print() {
  console.log(this.name);
}
console.log(window);

const obj = {
  name: "Object",
  print: function () {
    console.log(this.name);
  },
};
const obj2 = {
  name: "Object2",
  print,
};
//La función dentro del objeto 2 corresponderá al this del objeto, no el global.

obj.print();
obj2.print();

const obj3 = {
  name: "Object",
  print: () => {
    console.log(this.name);
  },
};
//Una arrow function mantiene el contexto al mismo nivel de la variable que la contiene.
//Es decir no crea un scope.
obj3.print();
function person(name) {
  this.name = name;
  this.name2 = name;
  //return console.log(this.name);
  //   return function () {
  //     console.log(this.name,this.name2);
  //   };

  //Esto se usaba antes de Enma Script 6.
  /* const that = this;
  return function(){
      console.log(that.name);
  }*/

  return () => console.log(this.name);
}
let jhan = new person("jhan");

jhan();
