let name = "Jony";
let hi = "hi";
let lorem =
  "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas fugiat repellat saepe sit tempora, impedit officia! Temporibus, recusandae? Soluta numquam repudiandae sit architecto laudantium impedit, a consectetur mollitia maxime modi. ";

//Contactenación.
let sayingHi = name + " " + hi;
console.log(sayingHi);

//Interpolación de variables.
//Template String
let sayingHis = `${name} ${hi}`;

console.log(sayingHis);

let stages =
  "<ul><li>Primavera</li><li>Verano</li><li>Invierno</li><li>Otoño</li></ul>";
console.log(stages);

let ul2 = `<ul>
<li>Primavera</li>
<li>Verano</li>
<li>Invierno</li>
<li>Otoño</li>
</ul>`;
console.log(ul2);

let stages3 =`<ul>`;
stages3 += "</ul>";
console.log(stages3);
