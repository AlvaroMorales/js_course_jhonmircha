//const wset= new WeakSet([1,2,3,4,4])

const wset = new WeakSet();

let val1 = { "val1": "1" };
let val2 = { "val2": "2" };
let val3 = { "val3": "3"};
let val4 = { val4: "4" };

wset.add(val1);
wset.add(val2);
wset.add(val3); 
wset.add(val4);

console.log(wset);

setInterval(() => console.log(wset), 500);
 
setTimeout(() => {
  val1 = null;
  val2 = null;
  val3 = null;
  val4 = null;
  console.log("adfadf");
}, 3000);

const wmap = new WeakMap();

let vm1 = {};
let vm2 = {};
let vm3 = { val3: 3 };
let vm4 = { val4: 4 };

wmap.set(vm1, "1");
wmap.set(vm2, "2");

console.log(wmap);

setInterval(() => console.log(wmap), 100000);

setTimeout(() => {
  vm1 = null;
}, 500000);
