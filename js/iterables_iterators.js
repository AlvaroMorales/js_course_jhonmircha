const iterable = [1, 2, 3, 4];

//Iterador del iterable.
const iterator = iterable[Symbol.iterator]();

console.log(iterator);
console.log(iterable);

let next = iterator.next();

while (!next.done) {
  console.log(next.value);
  next = iterator.next();
}
