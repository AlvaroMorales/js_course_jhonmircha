function AnimalWithoutFunctions(name, gender) {
  this.name = name;
  this.gender = gender;
}
//Métodos agregados al prototipo de la función constructura.
AnimalWithoutFunctions.prototype.make = function () {
  console.log("Jao");
};
AnimalWithoutFunctions.prototype.sayingHello = function () {
  console.log(`My name's ${this.name}`);
};

function Dog(name, gender, size) {
  this.super = AnimalWithoutFunctions;
  this.super(name, gender);
  this.size = size;
}
//Dog is inheriting from animalwithoutfunctions.
Dog.prototype = new AnimalWithoutFunctions();
Dog.prototype.constructor = Dog;

//Sobreescribir
Dog.prototype.make = function () {
  console.log("I am a dog and I like barking.");
};
Dog.prototype.barking = () => console.log("Wao, guau!");

const chihuahua = new Dog("Nelson", 0, 100);
console.log(chihuahua.name);
chihuahua.make();
chihuahua.barking();
chihuahua.sayingHello();
console.log(chihuahua);